// Help Firefox fit SVG elements to cell height
_.each(document.querySelectorAll('.fill-cell-height'), function(element) {
    element.style.height = element.parentElement.clientHeight + 'px';
});


d3.select('#logo-main').call(logo());
d3.select('#logo-mini').call(logo());
d3.select('#wavy').call(wavy());
d3.select('#composing').call(composing());
d3.select('#constellation').call(constellation());

_.each(document.querySelectorAll('.eval'), function(element) {
    eval(element.textContent);
});

var header = document.getElementById('fixed-header');
var inversionStart = document.getElementById('constellation-section');
window.addEventListener('scroll', function() {
    var invert = inversionStart.getBoundingClientRect().top <= header.getBoundingClientRect().height;
    d3.select(header).classed('invert', invert);
});
