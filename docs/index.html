<!doctype html>
<html>
    <head>
        <title>c3.js</title>
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:200|Open+Sans:300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="lib/highlight/styles/default.css">
        <link rel="stylesheet" type="text/css" href="tex-gyre-cursor-font.css">
        <link rel="stylesheet" type="text/css" href="main.css">
        <script src="../lib/d3.js" charset="utf-8"></script>
        <script src="../lib/underscore-1.4.4.js"></script>
        <script src="../c3.js"></script>
        <script src="lib/highlight/highlight.pack.js"></script>
    </head>
    <body>
        <header id="fixed-header" class="page-gutter">
            <svg id="logo-mini"></svg>
            <h1>c3.js</h1>
        </header>
        <div id="splash" class="center">
            <svg id="logo-main"></svg>
        </div>
        <svg id="wavy" class="fill-width"></svg>
        <section>
            <div class="page-gutter accent">
                <h2>What is c3?</h2>
                <p>c3 is a JavaScript library built on top of the visualisation library, <a href="http://d3js.org/">d3</a>. It lets you build charts with a minimal amount of code, while maintaining the full power and flexibility of d3 should you need it.</p>
                <p>This is all it takes to draw a simple line plot:</p>
            </div>
            <div class="table-layout fill-width">
                <div class="table-cell medium-width example-chart">
                    <svg id="example-basic" class="fill-cell-height"></svg>
                </div>
                <div class="table-cell example-code">
<pre><code class="eval">var data = [[0, 1], [1, 3], [2, 2], [3, 5], [4, 1], [5, 3]];
// Use a linePlot and configure it with data
var chart = c3.linePlot().data(data);
// Draw the chart into the element
chart(d3.select('#example-basic'));
</code></pre>
                </div>
            </div>
            <div class="page-gutter accent">
                <p>A chart in c3 is simply a function that can be called on a container element to render the chart within that element.</p>
            </div>
        </section>
        <div class="page-horizontal-gutter">
            <div><svg id="composing" class="fill-width"></svg></div>
        </div>
        <section>
            <div class="page-gutter">
                <h2>Composing</h2>
                <p>The power of c3 is in its component system. c3 provides a way to easily combine various components together to build complex charts. For example, you can use <code>c3.layerable</code> to place multiple plots on top of each other:</p>
            </div>
            <div class="table-layout fill-width">
                <div class="table-cell medium-width example-chart">
                    <svg id="example-layerable" class="fill-cell-height"></svg>
                </div>
                <div class="table-cell example-code">
<pre><code class="eval">var data = [[0, 1], [1, 3], [2, 2], [3, 5], [4, 1], [5, 3]];
var chart = c3.layerable().data(data)
    .addLayer('grid', c3.gridLines())
    .addLayer('area-plot', c3.areaPlot())
    .addLayer('line-plot', c3.linePlot());
chart(d3.select('#example-layerable'));
</code></pre>
                </div>
            </div>
            <div class="page-gutter">
                <p>In the above example, the component <code>c3.linePlot</code> becomes a sub-component of <code>c3.layerable</code> and is able to inherit its data from its parent. This is the same for the grid and the area plot, saving you from repeating the data as configuration for each of those components.</p>
            </div>
        </section>
        <section>
            <div class="page-gutter">
                <h2>Build components, not charts</h2>
                <p>c3 aims to help you write code that is reusable by default. Because everything in c3 is a component - often made up of many sub-components - you can easily turn "charts" into components. If the combination of line and area plots is a pattern you want to repeat, turning it into a reusable component is as simple as wrapping it in a function:</p>
            </div>
            <div class="table-layout fill-width">
                <div class="table-cell medium-width example-chart">
                    <svg id="example-component" class="fill-cell-height"></svg>
                </div>
                <div class="table-cell example-code">
<pre><code class="eval">// Define the component
var lineAndAreaPlot = function() {
    return c3.layerable()
        .addLayer('area-plot', c3.areaPlot())
        .addLayer('line-plot', c3.linePlot());
};
// Use the component to create multiple series
var chart = c3.layerable().xDomain([0, 5]).yDomain([0, 6])
    .addLayer('grid', c3.gridLines())
    .addLayer('series-1', lineAndAreaPlot().data([[0, 0], [2, 4], [4, 3], [5, 0]]))
    .addLayer('series-2', lineAndAreaPlot().data([[0, 0], [1, 3], [3, 2], [5, 0]]))
    .addLayer('series-3', lineAndAreaPlot().data([[0, 0], [2, 1], [4, 4], [5, 0]]));
chart(d3.select('#example-component'));
</code></pre>
                </div>
        </section>
        <section id="constellation-section" class="invert">
            <div class="page-gutter">
                <svg id="constellation" class="fill-width"></svg>
                <h2>Diving deeper</h2>
                <p>While c3 provides some ready-made components for charting purposes, a unique design will often require something more. Rather than starting from scratch, you can reuse some of the lower-level components c3 itself uses.</p>
                <h3>c3.drawable</h3>
                <p>This component handles the data-binding that is core to d3. Given an array of data, it maps each item in the array to an element in the document. <code>c3.drawable</code> automatically adds and removes elements to match the data, triggering 3 events in the process:</p>
                <ol>
                    <li><code>enter</code>, which is passed the elements that were added.</li>
                    <li><code>exit</code>, which is passed the elements that were removed.</li>
                    <li><code>update</code>, which is passed all elements that were not removed.</li>
                </ol>
                <p>You can configure <code>c3.drawable</code> to add any element, whether it is SVG or HTML. The following example uses a drawable to render a todo list based on data. The drawable is configured to automatically add (and remove) necessary <code>&lt;li&gt;</code> elements, while handlers are bound to the <code>enter</code> and <code>update</code> events to render the checkboxes and labels for each item.</p>
            </div>
            <div class="table-layout fill-width">
                <div class="table-cell medium-width example-chart invert">
                    <ul id="example-drawable"></ul>
                </div>
                <div class="table-cell example-code invert">
<pre><code class="eval">var data = [
    { title: 'Bread', done: false },
    { title: 'Cereal', done: true },
    { title: 'Confectionary', done: false },
    { title: 'Cordial', done: false },
    { title: 'Eggs', done: true },
    { title: 'Fruit', done: false },
    { title: 'Milk', done: false }
];
var todoList = c3.drawable().data(data)
    .elementTag('li')
    .enter(function(event) {
        var selection = event.selection;
        selection.append('input').attr('type', 'checkbox');
        selection.append('label');
    })
    .update(function(event) {
        var selection = event.selection;
        selection.select('label').text(function(d) { return d.title; });
        selection.select('input').property('checked', function(d) { return d.done; });
    });
todoList(d3.select('#example-drawable'));
</code></pre>
                </div>
            </div>
            <div class="page-gutter">
                <h3>c3.plottable</h3>
                <p>Often data is displayed visually by plotting it along x and y axes. <code>c3.plottable</code> simplifies this process by generating functions that convert data items into x and y coordinates, scaling the data to fit the available width and height. It does this by finding the minimum and maximum values of the data, and the width and height of the selection.</p>
            </div>
            <div class="table-layout fill-width">
                <div id="example-plottable" class="table-cell medium-width example-chart example-output invert"></div>
                <div class="table-cell example-code invert">
<pre><code class="eval">var data = [[0, 1], [1, 3], [2, 2], [3, 5], [4, 1], [5, 3]];
var selection = d3.select('#example-plottable');
var chart = c3.plottable().data(data).selection(selection);
var x = chart.x();
var y = chart.y();
// x and y convert any data item into x and y coordinates,
// e.g: for the data item [1, 3]:
selection.append('p').text('x: ' + x([1, 3]));
selection.append('p').text('y: ' + y([1, 3]));
</code></pre>
                </div>
            </div>
            <div class="page-gutter">
                <p>Note that the <code>selection</code> is being explicitly configured here by calling <code>chart.selection(selection)</code>. In previous examples, the <code>selection</code> was being configured as the chart was being called: <code>chart(selection)</code>. As we are using the <code>c3.plottable</code> without calling it (the plottable by itself does nothing when called, we are only interested in its generated <code>x</code> and <code>y</code> functions), the <code>selection</code> must be configured in this way.</p>
            </div>
            <!--
            <div class="page-gutter">
                <p>By default, it assumes your data is an array of coordinate pairs - e.g. <code>[[0, 1], [1, 2]]</code> - but this, like most aspects of c3, can be easily configured.</p>
                <p>The following example plots color temperatures along the x axis by representing the colors as points in a gradient. As the data doesn't represent coordinate pairs, we configure the <code>xAccessor</code> to point to the temperature values.</p>
            </div>
            <div id="example-plottable"></div>
            <div class="example-code invert">
                <pre><code class="">
var colorTemperatures = [
    { temperature: 1700, color: '#FF7900' },
    { temperature: 5000, color: '#FFE4CE' },
    { temperature: 6200, color: '#FFF5F5' },
    { temperature: 10500, color: '#E4EAFF' },
    { temperature: 27000, color: '#A8C5FF' }
];
var gradient = c3.plottable()
    .data(colorTemperatures)
    .xAccessor(function(d) { return d.temperature; })
    // Extending a component with a function means the function will be run
    // when the component is called
    .extend(function() {
        var x = this.x(); // c3.plottable's calculated x function
        var gradientCss = "linear-gradient(to right, ";
        gradientCss += this.data().map(function(d) {
            return d.color + ' ' + x(d) + 'px';
        }).join(', ');
        gradientCss += ")";
        this.selection().style('background', gradientCss);
    });
gradient(d3.select('#example-plottable-1'));
            </div>
            -->
            <div class="page-gutter">
                <h3>Mixing</h3>
                <p>You may have noticed that most charts would require the functionality of both <code>c3.drawable</code> - for inserting elements representing each data point - and <code>c3.plottable</code> - for figuring out where to position the elements.</p>
                <p>c3's component system supports mixins - a way to combine 2 (or more) components together to create a new component with the behaviour and configuration options of both. This is achieved through the <code>extend()</code> function, available on every component:</p>
            </div>
<pre><code class="example-code invert page-gutter">var chart = c3.component()
    .extend(c3.drawable())
    .extend(c3.plottable());
</code></pre>
            <div class="page-gutter">
                <p>This lets you configure or access properties and events defined on either of the mixed-in components:</p>
            </div>
<pre><code class="example-code invert page-gutter">var chart = c3.component()
    .extend(c3.drawable())
    .extend(c3.plottable())
    .elementTag('circle') // elementTag is from c3.drawable
    .update(function(event) { // update is from c3.drawable
        event.selection
            .attr('cx', this.x()) // x is from c3.plottable
            .attr('cy', this.y()) // y is from c3.plottable
            .attr('r', 4);
    });
</code></pre>
            <div class="page-gutter">
                <p>There might not be a lot of code there, but by wrapping the above snippet in a function to make it reusable, and turning the hardcoded radius of <code>4</code> into a configurable value, we arrive at the code for <code>c3.circlePlot</code>, a built-in component for plotting data as circles:</p>
            </div>
<pre><code class="example-code invert page-gutter">c3.circlePlot = function() {
    return c3.component('circlePlot')
        .extend(c3.drawable())
        .extend(c3.plottable())
        .elementTag('circle')
        .update(function(event) {
            event.selection
                .attr('cx', this.x())
                .attr('cy', this.y())
                .attr('r', this.radiusAccessor());
        })
        .extend({
            radiusAccessor: c3.prop(function() { return 4; })
        });
};
</code></pre>
        </section>
            <div class="page-gutter center">
                <p>Check out the source on <a href="https://bitbucket.org/dtang/c3/src">Bitbucket</a>.</p>
            </div>

        <script>hljs.initHighlightingOnLoad();</script>
        <script src="logo.js"></script>
        <script src="wavy.js"></script>
        <script src="composing.js"></script>
        <script src="constellation.js"></script>
        <script src="main.js"></script>
    </body>
</html>