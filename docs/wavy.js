var wavy = function() {
    var data1 = [
        [0, 4],
        [1, 0],
        [2, 9],
        [3, 2],
        [4, 13],
        [5, 0],
        [6, 4],
        [7, 8],
        [9, 2],
        [10, 16],
        [11, 0]
    ];
    var data2 = [
        [0, 0],
        [1, 12],
        [2, 0],
        [3, 8],
        [4, 3],
        [5, 13],
        [6, 0],
        [7, 2],
        [8, 7],
        [9, 10],
        [10, 2],
        [11, 8]
    ];

    return c3.component('wavy')
        .extend(c3.layerable())
        .xDomain([0, 11])
        .yDomain([0, 13])
        .addLayer('layer1',
            c3.areaPlot()
                .areaConstructor(function() {
                    return d3.svg.area().interpolate('basis');
                })
                .data(data1)
        )
        .addLayer('layer2',
            c3.areaPlot()
                .areaConstructor(function() {
                    return d3.svg.area().interpolate('basis');
                })
                .data(data2)
        );
};
