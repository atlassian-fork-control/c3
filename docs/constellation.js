var constellation = function() {
    var data = {
        points: {
            a: [2, 4],
            b: [7, 10],
            c: [1, 8],
            d: [2.5, 7],
            e: [3, 3],
            f: [8, 2],
            g: [7, 6],
            h: [3, 6],
            i: [3.5, 1],
            j: [5, 9.5]
        },
        lines: [
            ['a', 'b'],
            ['c', 'e'],
            ['d', 'f'],
            ['d', 'b'],
            ['d', 'a'],
            ['e', 'b'],
            ['a', 'c'],
            ['g', 'e'],
            ['g', 'c'],
            ['h', 'i'],
            ['h', 'a'],
            ['i', 'j'],
            ['j', 'f']
        ]
    };

    var xCoords = _.map(data.points, c3.defaults.x);
    var yCoords = _.map(data.points, c3.defaults.y);
    var charCodes = _.map(data.points, function(d, i) { return i.charCodeAt(0); });
    var colors = [
        'hsl(0, 80%, 90%)',
        'hsl(0, 98%, 78%)',
        'hsl(203, 100%, 83%)',
        'hsl(190, 100%, 83%)',
        'hsl(170, 100%, 83%)'
    ];
    var lineColorScale = d3.scale.ordinal()
        .range(colors);
    var pointColorScale = d3.scale.linear()
        .domain([d3.min(xCoords), d3.max(xCoords)])
        .range(['hsl(0, 100%, 80%)', 'hsl(190, 100%, 83%)']);

    var lines = c3.component('constellation-lines')
        .extend(c3.drawable())
        .extend(c3.plottable())
        .elementTag('line')
        .data(data.lines)
        .xDomain([d3.min(xCoords), d3.max(xCoords)])
        .yDomain([d3.min(yCoords), d3.max(yCoords)])
        .update(function(event) {
            var points = data.points;
            var xScale = this.xScale();
            var yScale = this.yScale();
            event.selection
                .attr('x1', function(d) {
                    return xScale(points[d[0]][0]);
                })
                .attr('y1', function(d) {
                    return yScale(points[d[0]][1]);
                })
                .attr('x2', function(d) {
                    return xScale(points[d[1]][0]);
                })
                .attr('y2', function(d) {
                    return yScale(points[d[1]][1]);
                })
                .attr('stroke', function(d) {
                    var n = ~~d3.mean(d, function(d) { return d.charCodeAt(0); });
                    return lineColorScale(n);
                });
        });
    var points = c3.component('constellation-points')
        .extend(c3.circlePlot())
        .data(_.map(data.points, _.identity))
        .radiusAccessor(function() { return 5 })
        .update(function(event) {
            event.selection.attr('fill', function(d) {
                return pointColorScale(d[0]);
            });
        });

    return c3.component('constellation')
        .extend(c3.layerable())
        .addLayer('points', points)
        .addLayer('lines', lines);
};