/**
 * Plot an array of points as circles
 */
c3.circlePlot = function() {
    return c3.component('circlePlot')
        .extend(c3.drawable())
        .extend(c3.plottable())
        .elementTag('circle')
        .update(function(event) {
            event.selection
                .attr('cx', this.x())
                .attr('cy', this.y())
                .attr('r', this.radiusAccessor());
        })
        .extend({
            radiusAccessor: c3.prop(function() { return 4; })
        });
};
