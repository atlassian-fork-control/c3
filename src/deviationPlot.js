c3.deviationPlot = function() {
    return c3.component('deviationPlot')
        .extend(c3.drawable())
        .extend(c3.plottable())
        .elementTag('path')
        .elementClass('area')
        .dataFilter(function(data) {
            return [data];
        })
        .update(function(event) {
            var yAccessor = this.yAccessor();
            var deviationAccessor = this.deviationAccessor();
            var yScale = this.yScale();
            var area = d3.svg.area()
                .x(this.x())
                .y0(function(d) {
                    return yScale(yAccessor(d) + deviationAccessor(d));
                })
                .y1(function(d) {
                    return yScale(Math.max(yAccessor(d) - deviationAccessor(d), 0));
                });

            event.selection.attr('d', area(this.data()));
        })
        .extend({
            deviationAccessor: c3.prop(function(d) { return d[2]; })
        });
};
