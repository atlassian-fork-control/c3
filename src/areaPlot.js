/**
 * Plot an array of points as an area under the points
 */
c3.areaPlot = function() {
    return c3.component('areaPlot')
        .extend(c3.drawable())
        .extend(c3.plottable())
        .elementTag('path')
        .elementClass('area')
        .dataFilter(function(data) {
            return [data];
        })
        .extend({
            areaConstructor: c3.prop(d3.svg.area)
        })
        .update(function(event) {
            var area = this.areaConstructor()()
                .x(this.x());
            if (this.cartesian()) {
                area
                    .y0(this.height())
                    .y1(this.y());
            } else {
                area
                    .y0(this.y())
                    .y1(0);
            }

            event.selection.attr('d', area(this.data()));
        });
};
