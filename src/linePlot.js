/**
 * Plot an array of points as a line
 */
c3.linePlot = function() {
    return c3.component('linePlot')
        .extend(c3.drawable())
        .extend(c3.plottable())
        .elementTag('path')
        .elementClass('line')
        .dataFilter(function(data) {
            return [data]; // Map all of the data values to a single element
        })
        .extend({
            lineConstructor: c3.prop(d3.svg.line)
        })
        .update(function(event) {
            var line = this.lineConstructor()()
                .x(this.x())
                .y(this.y());

            event.selection.attr('d', line(this.data()))
                .attr('stroke', 'black')
                .attr('fill', 'none');
        });
};
