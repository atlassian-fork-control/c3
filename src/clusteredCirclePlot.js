/*
 * Author: Patrick Teen (Atlassian) 2013
 */
(function (figue) {
    if (!figue) return;
    
    function _internalXAccessor(d) { return d.point[0]; }
    function _internalYAccessor(d) { return d.point[1]; }
    function _buildPointFromTree(tree) {
        var point = _.extend({}, {
            point: [ 
                this.xScale().invert(tree.centroid[0]), 
                this.yScale().invert(tree.centroid[1])
            ],
            size: tree.size,
            distance: tree.dist,
            isCluster: false
        });
        if (tree.left || tree.right) {
            point.isCluster = true;
        }
        point.label = _resolveAllChildLabels(tree);
        return point;
    }
    function _resolveAllChildLabels(tree) {
        var result = [];
        if (tree && tree.label == -1) {
            result.push.apply(result, _resolveAllChildLabels(tree.left));
            result.push.apply(result, _resolveAllChildLabels(tree.right));
        } else {
            result.push(tree.label);
        }
        return result;
    }
    function _pruneAgglomerate(tree, clusteredResult, unclusteredResult) {
        if (tree) {
            if (tree.dist && tree.dist > this.threshold()) {
                _pruneAgglomerate.call(this, tree.left, clusteredResult, unclusteredResult);
                _pruneAgglomerate.call(this, tree.right, clusteredResult, unclusteredResult);
            } else {
                var point = _buildPointFromTree.call(this, tree );
                point.isCluster ? clusteredResult.push(point) : unclusteredResult.push(point);
            }
        }
    }
    function _updateClustering() {
        var clustered = [],
            unclustered = [];

        var x = this.x(),
            y = this.y();

        var vectorLabels = _.map(this.data(), this.labelAccessor());
        var scaledVectors = _.map(this.data(), function(element) {
            return [
                x(element),
                y(element)
            ]
        }, this);

        var agglomerate = figue.agglomerate(vectorLabels, scaledVectors, figue.MAX_DISTANCE, figue.COMPLETE_LINKAGE);
        
        _pruneAgglomerate.call(this, agglomerate, clustered, unclustered);

        this.getLayer('clusteredLayer').data(clustered);
        this.getLayer('unclusteredLayer').data(unclustered);
    }
    c3.clusteredCirclePlot = function() {
        var clusteredCirclePlot = c3.component()
            .extend({
                clustered: c3.prop([]),
                unclustered: c3.prop([]),
                clusterRadiusAccessor: c3.prop(function() { return 10; }),
                singletonRadiusAccessor: c3.prop(function() { return 5; })
            });
        clusteredCirclePlot
            .extend(_updateClustering)
            .extend(c3.layerable()
                .addLayer('clusteredLayer', c3.circlePlot()
                    .xAccessor(_internalXAccessor)
                    .yAccessor(_internalYAccessor)
                    .elementClass('cluster')
                    .radiusAccessor(function (elem) {
                        return clusteredCirclePlot.clusterRadiusAccessor()(elem);
                    })
                )
                .addLayer('unclusteredLayer', c3.circlePlot()
                    .xAccessor(_internalXAccessor)
                    .yAccessor(_internalYAccessor)
                    .elementClass('singleton')
                    .radiusAccessor(function (elem) {
                        return clusteredCirclePlot.singletonRadiusAccessor()(elem);
                    })
                )
                .extend({
                    labelAccessor: c3.prop(function(element) { return element.issue; }),
                    threshold: c3.prop(10)
                })
        );
        return clusteredCirclePlot;
    };
}(window.figue));
