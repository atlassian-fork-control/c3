/**
 * The root c3 component constructor.
 *
 * A component is a function that can be applied to a d3 selection.
 *
 * A component can be applied to any number of selections.
 * 
 *    var myComponent = c3.component();
 *    myComponent(selection1);
 *    myComponent(selection2);
 *
 * When applied to a selection, the component has access to the selection through
 * this.selection().
 *
 * A component can be extended by adding properties before it is applied.
 * 
 *    var myComponent = c3.component().extend({
 *        color: c3.prop('red'),
 *        click: c3.event(),
 *        saySomething: function() { alert('Something!'); }
 *    });
 *
 * A component can also be extended by mixing in another component.
 * 
 *    var myComponent = c3.component().extend(otherComponent);
 *
 * A component can have a parent component, by setting the parent as property.
 *
 *    var parent = c3.component();
 *    var child = c3.component().parent(parent);
 *
 * This allows children components to access their parents' properties by
 * calling this.parent(), and to inherit parent properties through c3.inherit().
 *
 * @param {string} [displayName] - name to show when debugging
 */
c3.component = function(displayName) {
    var mixins = [];

    function makeComponentFunction() {
        var component = function (selection) {
            return component.applyTo(component, selection);
        };
        return component;
    }

    var component = makeComponentFunction();

    /**
     * Copy src into dest, but excluding keys in exclude
     * @param {function|object} dest
     * @param {function|object} src
     * @param {function|object} [exclude]
     */
    function selectiveCopy(dest, src, exclude) {
        // Can't use _.each as src can be a function, in which case
        // _.each will see src.length and think it's an array
        for (var key in src) {
            if (exclude && key in exclude) continue;
            dest[key] = src[key];
        }
        return dest;
    }

    var componentCommon = {
        displayName: displayName,
        selection: c3.prop(null),
        parent: c3.prop(null),
        /**
         * Applies a component to a selection, setting base as the context
         * @param {c3.component} base
         * @param {d3.selection} selection
         */
        applyTo: function(base, selection) {
            if (selection) component.selection(selection);
            _.each(mixins, function(mixin) {
                if (mixin.applyTo) {
                    mixin.applyTo(base, selection);
                } else {
                    mixin.call(base, selection);
                }
            });
            return component;
        },
        /**
         * Extend the component with additional behaviour and/or properties.
         * @param {c3.component|function|object} mixable
         */
        extend: function(mixable) {
            if (typeof mixable === 'function') {
                if (mixable.parent) {
                    mixable.parent(component);
                }
                mixins.push(mixable);
            }
            return selectiveCopy(component, mixable, componentCommon);
        }
    };

    return _.extend(component, componentCommon);
};
