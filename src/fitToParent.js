/**
 * Fits the component to the dimensions of it's parent node.
 */
c3.fitToParent = function () {
    return c3.component('fitToParent')
        .extend({
            width: c3.prop(),
            height: c3.prop()
        })
        .extend(function (){
            var domParent = this.selection().node().domNode.parentElement;

            this.height(c3.checkIsNumber(domParent.clientHeight));
            this.width(c3.checkIsNumber(domParent.clientWidth));
        });
};