/**
 * A component mixin that add a boolean `clipped` property (default: false)
 * to a c3.component. If set to true, sets a clipping path on the component
 * that prevents it's content from overflowing
 *
 */
c3.clippable = (function() {
    var clippingPathCounter = 0;

    return function () {
        return c3.component('clippable')
            .extend({
                clipped: c3.prop(false)
            })
            .extend(function() {
                if (!this.clipped()) return;

                var target = this;
                var clip = c3.singular()
                    .elementTag('clipPath')
                    .enter(function(event) {
                        var randomId = 'c3clip' + clippingPathCounter;
                        clippingPathCounter++;
                        target.selection().attr('clip-path', 'url(#' + randomId + ')');
                        event.selection
                            .attr('id', randomId)
                            .append('rect')
                            .attr('x', '0')
                            .attr('y', '0')
                            .attr('fill', 'none');
                    })
                    .update(function(event) {
                        event.selection.select('rect')
                            .attr('width', target.width())
                            .attr('height', target.height());
                    }); 
                clip(this.selection());
            });
    };
}());
