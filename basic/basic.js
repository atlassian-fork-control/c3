var data = [
	[1, 5],
	[2, 3],
	[3, 4],
	[4, 17],
	[5, 2],
	[6, 3],
	[7, 1],
	[8, 13],
	[9, 11],
	[10, 9],
	[11, 10]
];

var plots = c3.layerable()
	.addLayer('area-under-line', c3.areaPlot())
	.addLayer('line', c3.linePlot())
	.addLayer('points', c3.circlePlot());

var chart = c3.component('myChart')
	.extend(c3.ieDimensions())
	.extend(c3.borderLayout())
	.data(data)
	.center(plots)
	.south(c3.axis().height(30))
	.west(c3.axis().orient('left').width(30));

var svg = d3.select('#c3-chart').append('svg');

chart(svg);